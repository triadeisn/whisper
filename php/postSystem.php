<?php
/**
 * Created by PhpStorm.
 * User: beauv
 * Date: 13/11/2018
 * Time: 09:37
 */

function sendPost($user_id, $post) {
    global $db;
    
    if (isset($user_id) && !empty($user_id)) {
        if (isset($post) && !empty($post)) {
            $post = htmlspecialchars($post);
            
            $n_post = $db->prepare('INSERT INTO `post`(sender_id, content) VALUES(:creator, :post)');
            $n_post->execute([
                'creator' => $user_id,
                'post' => $post
            ]);
        }
    }
}