<?php
/**
 * Created by PhpStorm.
 * User: LordAll
 * Date: 05/11/2018
 * Time: 15:23
 */

include("C:/wamp64/www/isnproject/includes/init.php");

/*
 * State value possible:
 * 0 = Not friend
 * 1 = Already friend
 * 2 = Request sender
 * 3 = Request receiver
 */
function friendState($user_id, $user_id2) {
    global $db;
    $fetchRequest = $db->prepare("SELECT * FROM `friend` WHERE (`sender_id` = :sender AND `receiver_id` = :receiver) OR (`sender_id` = :receiver AND receiver_id = :sender)");
    $fetchRequest->execute([
        'sender' => $user_id,
        'receiver' => $user_id2
    ]);
    if ($fetchRequest->rowCount() != 1) {
        return 0;
    }
    else if ($fetchRequest-> rowCount() == 1) {
        $friendInfo = $fetchRequest->fetch();
        if ($friendInfo['state'] == 1) {
            return 1;
        }
        else {
            if ($friendInfo['sender_id'] == $user_id) {
                return 2;
            }
            else if ($friendInfo['receiver_id'] == $user_id) {
                return 3;
            }
        }
    }
}

// Use to send friend request
/*
 * Database construction:
 * id <int>[AI]
 * sender_id <int>
 * receiver_id <int>
 * state <int>[Default value = 0]
 *   * State :
 *      0 : Request send, not friend
 *      1 : Friend
 */
function sendRequest($sender_id, $receiver_id) {
    global $db;
    $date = date(now);
    $sendRequest = $db->prepare("INSERT INTO `friend`(`sender_id`, `receiver_id`, `date`) VALUE (:sender, :receiver, :dates)");
    $sendRequest->execute([
        'sender' => $sender_id,
        'receiver' => $receiver_id,
        'dates' => $date
    ]);
}

/*
 * Use to accept request
 * Update `state` in database to 1
 */
function acceptRequest($receiver_id, $sender_id) {
    global $db;
    $date = date(now);
    $acceptRequest = $db->prepare("UPDATE friend SET state = 1 WHERE receiver_id = :receiver AND sender_id = :sender AND `date` = :datea");
    $acceptRequest->execute([
        'receiver' => $receiver_id,
        'sender' => $sender_id,
        'datea' => $date
    ]);
}
/*
 * Use to delete friend invite and friend
 * Delete column
 */
function deleteFriend($user_id, $user_id2) {
    global $db;
    $deleteFriend = $db->prepare("DELETE FROM friend WHERE (sender_id = :user1 AND receiver_id = :user2) OR (sender_id = :user2 AND receiver_id = :user1)");
    $deleteFriend->execute([
        'user1' => $user_id,
        'user2' => $user_id2
    ]);
}

function blockedPeople($user_id, $user_id2) {
    global $db;
    $date = date(now);
    if (friendState($user_id, $user_id2) != 0) {
        deleteFriend($user_id, $user_id2);
    }
    $blockrequest = $db->prepare("INSERT INTO `block`(`blocker_id`, `blocked_id`, `date`) VALUE (:blocker, :blocked, :dateb)");
    $blockrequest->execute([
        'blocker' => $user_id,
        'blocked' => $user_id2,
        'dateb' => $date
    ]);
}