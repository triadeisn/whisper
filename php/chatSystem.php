<?php

//include_once("../includes/init.php");

function sendMessage($senderId, $receiverId, $message) {
    global $db;
    if(isset($message) AND !empty($message)) {
        $message =htmlspecialchars($message);
        $insertmsg = $db->prepare('INSERT INTO chat(sender_id,receiver_id,message) VALUES(:sender_id, :receiver_id, :msg)');
        $insertmsg->execute([
            'sender_id' => $senderId,
            'receiver_id' => $receiverId,
            'msg' => $message
        ]);
    }
}

function isSender($message_id, $user_id) {
    global $db;
    $fetch_message = $db->prepare('SELECT * FROM `chat` WHERE id = :id AND `sender_id` = :user_id');
    $fetch_message->execute([
        'id' => $message_id,
        'user_id' => $user_id
    ]);
    
    if ($fetch_message->rowCount() == 1) {
        return true;
    }
    else {
        return false;
    }
}

function lastChat($user_id) {
    global $db;
    
    $test = array();
    
    $fetch_msg = $db->prepare('SELECT * FROM `chat` WHERE sender_id = :user_id OR receiver_id = :user_id');
    $fetch_msg->execute([
        'user_id' => $user_id
    ]);
    
    while($saveId = $fetch_msg->fetch()) {
        if($saveId['sender_id'] == $user_id) {
            if(!in_array($saveId['receiver_id'], $test)) {
                $test[] = $saveId['receiver_id'];
            }
        }
        else if($saveId['receiver_id'] == $user_id) {
            if(!in_array($saveId['sender_id'], $test)) {
                $test[] = $saveId['sender_id'];
            }
        }
    }
    
    return $test;
}


/**
 * User 1 => $_SESSION['id]
 * User 2 => the other user
 */
function fetchLastMessage($user1, $user2) {
    global $db;
    
    $fetch_allmsg = $db->prepare('SELECT `message` FROM `chat` WHERE (`sender_id` = :user1 AND `receiver_id` = :user2) OR (`sender_id` = :user2 AND `receiver_id` = :user1) ORDER BY `send_time` DESC');
    $fetch_allmsg->execute([
        'user1' => $user1,
        'user2' => $user2
    ]);
    
    $lastMsg = $fetch_allmsg->fetch()[0];
    return $lastMsg;
}