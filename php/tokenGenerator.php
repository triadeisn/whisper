<?php

/*
 * TokenGenerator
 * Generate String with random char (number, letter)
 * $size = Size of the generate token
 */
function tokenGenerator($size) {
    $token = sha1(session_id().microtime());
    $generatedToken = substr($token, -1 * $size);
    return $generatedToken;
}