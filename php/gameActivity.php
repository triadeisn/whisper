<?php
/**
 * Created by PhpStorm.
 * User: beauv
 * Date: 13/11/2018
 * Time: 09:37
 */

include_once("C:/wamp64/www/isn/includes/init.php");
include("C:/wamp64/www/isn/php/tokenGenerator.php");

function createGameEvent($user_id, $game_id, $eventType) {
    global $db;

//    $description = htmlspecialchars($description);

    $eventToken = tokenGenerator(12);

    $create_event = $db -> prepare('INSERT INTO `game_act`(`user_id`, `game_id`, `event_token`, `event_type`) VALUE(:user_id, :game_id, :event_token, :event_type)');
    $create_event -> execute([
        'user_id' => $user_id,
        'game_id' => $game_id,
        'event_token' => $eventToken,
        'event_type' => $eventType,
//        'time_event' => $time,
//        'description' => $description
    ]);
}