<?php

/**
 * Use : check if user exist and fetch his id
 */
function isRegister($email, $password) {
    global $db, $error;
    
    $fetch_user = $db->prepare('SELECT `id` FROM `users` WHERE `email` = :email AND `password` = :password ');
    $fetch_user->execute([
        'email' => $email,
        'password' => $password
    ]);
    if ($fetch_user->rowCount() == 1) {
        $_SESSION['id'] = $fetch_user->fetch() ['id'];
        return true;
    }
    else {
        return false;
    }
}

/**
 * Use : Sign in on the site
 */
function signin($email, $password) {
    global $db, $error;
    if (!empty($email) && !empty($password))
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $s_password = sha1($password);
            $fetch_user = $db->prepare('SELECT * FROM `users` WHERE `email` = :email AND `password` = :password ');
            $fetch_user->execute(['email' => $email, 'password' => $s_password ]);
            if ($fetch_user->rowCount() == 1)
            {
                $_SESSION['id'] = $fetch_user->fetch() ['id'];
                header('Location: ..');

            }
            else {
                $error = "Alpha";
            }
        }
        else
        {
            $error = "Votre adresse mail n'est pas valide";
        }
    }
    else
    {
        $error = "Vous devez remplir tout les champs";
    }
}

/**
 * Use : Sign up on the site
 */
function signup($username, $email, $email2, $password, $password2) {
    global $db, $erreur;
    if (!empty($username) && !empty($email) && !empty($email2) && !empty($password) && !empty($password2))
    {
        $username = htmlspecialchars($username);
        $mail = htmlspecialchars($email);
        $mail2 = htmlspecialchars($email2);
        $mdp = sha1($password);
        $mdp2 = sha1($password2);
        $username_l = strlen($username);
        if ($username_l <= 255)
        {
            if ($mail == $mail2)
            {
                if (filter_var($mail, FILTER_VALIDATE_EMAIL))
                {
                    $reqmail = $db->prepare("SELECT * FROM `users` WHERE email = ?");
                    $reqmail->execute(array(
                        $mail
                    ));
                    $reqmail->execute(array(
                        $mail
                    ));
                    $mailexist = $reqmail->rowCount();
                    if ($mailexist == 0)
                    {
                        if ($mdp == $mdp2)
                        {
                            $insertmbr = $db->prepare("INSERT INTO `users`(username, email, password) VALUES(?,?,?)");
                            $insertmbr->execute(array(
                                $username,
                                $mail,
                                $mdp
                            ));
                            
                            if(isRegister($mail, $mdp)) {
                                $erreur = "votre compte a été enregistré !";
                                header('Location: signin.php');
                            }
                            else {
                                $erreur = "Une erreur est survenue";
                            }
                        }
                        else
                        {
                            $erreur = "Vos mots de passe ne correspondent pas !";
                        }
                    }
                    else
                    {
                        $erreur = "Adresse mail déjà utilisée !";
                    }
                }
                else
                {
                    $erreur = "Votre adresse mail n'est pas valide !";
                }
            }
            else
            {
                $erreur = "Vos adresses mail ne correspondent pas !";
            }
        }
        else
        {
            $erreur = "votre pseudo ne doit pas dépasser 255 caractères !";
        }
    }
    else
    {
        $erreur = "tous les champs doivent être complétés";
    }
}

/**
 * Use : Fetch the username
 */
function getUsername($user_id) {
    global $db;
    
    $fetch_name = $db->prepare('SELECT `username` FROM `users` WHERE `id` = :user_id');
    $fetch_name->execute([
        'user_id' => $user_id
    ]);
    
    if($fetch_name->rowCount() == 1) {
        return $fetch_name->fetch()["username"];
    }
    
}

function getProfilepicture($user_id) {
    global $db;
    
    $fetch_url = $db->prepare('SELECT `profile_pic` FROM `users` WHERE `id` = :user_id');
    $fetch_url->execute([
        'user_id' => $user_id
    ]);
    
    if($fetch_url->rowCount() == 1) {
        return $fetch_url->fetch()["profile_pic"];
    }
}