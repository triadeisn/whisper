<?php

include_once("../includes/init.php");

function sendMessage($senderId, $receiverId, $message) {
    global $db;
    if(isset($message) AND !empty($message)) {
        $message =htmlspecialchars($message);
        $insertmsg = $db->prepare('INSERT INTO chat(sender_id,receiver_id,message) VALUES(:sender_id, :receiver_id, :msg)');
        $insertmsg->execute([
            'sender_id' => $senderId,
            'receiver_id' => $receiverId,
            'msg' => $message
        ]);
    }
}

function isSender($message_id, $user_id) {
    global $db;
    $fetch_message = $db->prepare('SELECT * FROM `chat` WHERE id = :id AND `sender_id` = :user_id');
    $fetch_message->execute([
        'id' => $message_id,
        'user_id' => $user_id
    ]);
    
    if ($fetch_message->rowCount() == 1) {
        return true;
    }
    else {
        return false;
    }
}