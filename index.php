<?php
	session_start();
	include_once("includes/init.php");
	include_once("php/function.php");
	?>
<html>
	<head>
		<title>Whisper
		</title>
		<meta charset="utf-8" name="viewport" content="width=device-witch, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
		<script src="../js/jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<header>
			<nav>
				<ul id="left">
					<li>
                        <a id="profile">
                            <i class="fas fa-user"></i>
                            <?php echo " " . $language[findLang()]['navbar']['profile']; ?>
						</a>
                    </li>
					<li>
						<a id="message">
						<i class="fas fa-comment-alt"></i>
                        <?php echo " " . $language[findLang()]['navbar']['message']; ?>
						</a>
					</li>
					<li>
						<a id="friend">
						<i class="fas fa-user-friends">
						</i>
                         <?php echo " " . $language[findLang()]['navbar']['friend']; ?>
						</a>
					</li>
				</ul>
				<ul id="center">
					<li id="center">
						<a href="#">
						<img style="display: inline;" src="img/logo.png">
						</a>
					</li>
				</ul>
			</nav>
		</header>
		<main>
			<div class="wall-box">
				<section class="status-form">
					<div class="section-name">
						<span> <?php echo $language[findLang()]['main']['add_post']; ?>
						</span>
					</div>
					<hr>
					<form>
						<div contenteditable="true" class="text-area">
						</div>
						<input type="submit" name="status" value=" <?php echo $language[findLang()]['main']['post']; ?>">
					</form>
				</section>
			</div>
		</main>
		<footer>
			<form method="get" id="langue_change" class="formfield-select--container">
				<select name="lang" onchange="changeLanguage()">
					<?php if (isset($_GET['lang']) AND $_GET['lang'] == 'en') { ?>
					<option value="en">English
					</option>
					<option value="fr">Français
					</option>
					<?php } else { ?>
					<option value="fr">Français
					</option>
					<option value="en">English 
					</option>
					<?php } ?>
				</select>
			</form>
		</footer>
        <!-- profile - Modal -->
		<div id="profileModal" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<span id="closeprofile" class="close">&times;
					</span>
					<h1> <?php echo $language[findLang()]['modal']['profile']['header']; ?>
					</h1>
				</div>
				<div class="modal-body">
					
				</div>
			</div>
		</div>
		<!-- message - Modal -->
		<div id="messageModal" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<span id="closemessage" class="close">&times;
					</span>
					<h1> <?php echo $language[findLang()]['modal']['message']['header']; ?>
					</h1>
				</div>
				<div class="modal-body">
					<div class="modal-user">
						<?php
                        if (lastChat($_SESSION['id']) == 0) {
                            echo "Pas de conversation";
                        }
                        else {
                            $userid = lastChat($_SESSION['id']);
							for($i = 0; $i < count($userid); $i++) {
							?>
						<a href="chat/?id=<?php echo $userid[$i]; ?>">
							<ul>
								<li>
									<img class="profile-picture" src="<?php echo getProfilepicture($userid[$i]); ?>">
								</li>
								<li class="profile-information">
									<span>
									<?php echo getUsername($userid[$i]); ?>
									</span>
									<br>
									<span class="time">
                                        <?php
                                            echo fetchLastMessage($_SESSION['id'], $userid[$i]);;
                                        ?>
                                    </span>
								</li>
							</ul>
						</a>
						<?php
							}
                            }
							?>
					</div>
				</div>
			</div>
		</div>
        <!-- friend - Modal -->
		<div id="friendModal" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<span id="closefriend" class="close">&times;
					</span>
					<h1> <?php echo $language[findLang()]['modal']['friend']['header']; ?>
					</h1>
				</div>
				<div class="modal-body">
					
				</div>
			</div>
		</div>
		<script type="text/javascript">
			function changeLanguage() {
			  document.getElementById("langue_change").submit();
			}
            
            /* profile */
            var profile_modal = document.getElementById('profileModal');
            var profile_btn = document.getElementById("profile");
            var profile_span = document.getElementById("closeprofile");
            profile_btn.onclick = function() {
                profile_modal.style.display = "block";
                profile_btn.classList.add('active');
			}
			profile_span.onclick = function() {
                profile_modal.style.display = "none";
                profile_btn.classList.remove('active');
			}
            
            /* message */
			var message_modal = document.getElementById('messageModal');
			var message_btn = document.getElementById("message");
			var message_span = document.getElementById("closemessage");
			message_btn.onclick = function() {
                message_modal.style.display = "block";
                message_btn.classList.add('active');
            }
            message_span.onclick = function() {
                message_modal.style.display = "none";
                message_btn.classList.remove('active');
            }
			
            /* friend */
            var friend_modal = document.getElementById('friendModal');
			var friend_btn = document.getElementById("friend");
			var friend_span = document.getElementById("closefriend");
			friend_btn.onclick = function() {
                friend_modal.style.display = "block";
                friend_btn.classList.add('active');
            }
            friend_span.onclick = function() {
                friend_modal.style.display = "none";
                friend_btn.classList.remove('active');
            }
            
            /* Click on the windows */
            window.onclick = function(event) {
                if (event.target == message_modal) {
                    message_modal.style.display = "none";
                    message_btn.classList.remove('active');
                }
                else if (event.target == profile_modal) {
                    profile_modal.style.display = "none";
                    profile_btn.classList.remove('active');
                }
                else if (event.target == friend_modal) {
                    friend_modal.style.display = "none";
                    friend_btn.classList.remove('active');
                }
            }
		</script>
	</body>
</html>