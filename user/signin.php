<?php
session_start();
include_once("../includes/init.php");
include_once("../php/function.php");

if(isset($_POST['form'])) {
    signin($_POST['username'], $_POST['password']);
}

if(isset($_SESSION['id'])) {
    echo "ALPHA TANGO CHARLIE";
}
else if(isset($error)) {
    echo $error;
}
?>
<htlm>
    <head>
        <title>Connexion</title>
        <link rel="stylesheet" type="text/css" href="../css/login.css">
    </head>
    <body>
        <div id="wrapper">

	       <form name="login-form" class="login-form" action="" method="post">
	
		      <div class="header">
		          <h1><?php echo $language[findLang()]['user']['login']['logging']; ?></h1>
		          <span><?php echo $language[findLang()]['user']['login']['fill_out']; ?></span>
		      </div>
	
		      <div class="content">
		          <input name="username" type="text" class="input username" placeholder="<?php echo $language[findLang()]['user']['login']['username']; ?>" />
		          <div class="user-icon"></div>
		          <input name="password" type="password" class="input password" placeholder="<?php echo $language[findLang()]['user']['login']['password']; ?>" />
		          <div class="pass-icon"></div>		
		      </div>

		      <div class="footer">
		          <input type="submit" name="form" value="<?php echo $language[findLang()]['user']['login']['login']; ?>" class="button" />
                  <a class="register" href="signup.php"><?php echo $language[findLang()]['user']['login']['register']; ?></a>
		      </div>
	
	       </form>
            
        </div>
        <div class="gradient"></div>
    </body>
</htlm>