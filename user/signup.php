<?php
include_once('../includes/init.php');
include_once("../php/userSystem.php");


if(isset($_POST['formeinscription'])) {
    signup($_POST['pseudo'], $_POST['mail'], $_POST['mail2'], $_POST['mdp'], $_POST['mdp2']);
}
?>

<html>
    <head>
         <title>Formulaire</title>
         <meta charset=utf-8>
    </head>
    <body>
        <div align="center">
            <h2><?php echo $language[findLang()]['user']['registration']['registration']; ?></h2>
            <br /><br /><br />
            <form method="POST" action="">
                
        <table>
             <tr>
            <td align="right">
                <label for="pseudo">Pseudo :</label>
            </td>
            <td>
                <input type="text"placeholder="votre pseudo" id="pseudo" name="pseudo" />
            </td>
             </tr>
             <tr>
            <td align="right">
                <label for="mail">Mail :</label>
            </td>
            <td>
                <input type="email"placeholder="votre mail" id="mail" name="mail" />
            </td>
             </tr>
             <tr>
            <td align="right">
                <label for="mail2">Confirmation du mail :</label>
            </td>
            <td>
                <input type="email"placeholder="Confirmez votre mail" id="mail2" name="mail2" />
            </td>
             </tr>
             <tr>
            <td align="right">
                <label for="mdp">Mot de passe :</label>
            </td>
            <td>
                <input type="password"placeholder="Mot de passe" id="mdp" name="mdp"/>
            </td>
              </tr>
             <tr>
            <td align="right">
                <label for="mdp2">Confirmation du mot de passe :</label>
            </td>
            <td>
                <input type="password"placeholder="Confirmation mot de passe" id="mdp2" name="mdp2"/>
            </td>
             </tr>
            <tr>
            <td></td>
            <td align="center">
                <br />
                <input type="submit" name="formeinscription" value="S'insrcrire" />
            </td>
            </tr>
        </table>
            </form>
            <?php
            
            if(isset($erreur))
            {
                echo '<font color="red">' . $erreur . "</font>";
            }
            ?>
        </div>
    </body>
</html>