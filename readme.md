# [Project name]
----------
### User system :
* signin($email, $password) :

    Use when the user want connect to the website.
    
* signup($pseudo, $email, $email2, $password, $password2) :

    Use to register user on this website.

### Friend system :
* friendState($user_id, $user_id2) :

    This function returns the state of a friend request, it's used to show the right button, for example the "accept" button
    * 0 = Not a friend
    * 1 = Already a friend
    * 2 = Request sender
    * 3 = Request receiver
    
* sendRequest($sender_id, $receiver_id) :

    This function is used to send a request to other users
    * $sender_id : The ID of the sender
    * $receiver_id : The ID of the receiver
    `sendRequest($_SESSION['id'], $userInfo['id'])`
    
    Database :
    * id : ID of the request (int in auto increment)
    * sender_id : ID of the sender of request
    * receiver_id : ID of the receiver of request
    * state : State of the request
        * 0 : Request sent, not accepted for the moment (default value)
        * 1 : Request accepted
        
* acceptRequest($sender_id, $receiver_id):
    * $sender_id : The ID of the sender
    * $receiver_id : The ID of the receiver
    
    `acceptRequest($userInfo['id'], $_SESSION['id'])`

    Database :
    * `UPDATE friend SET state = 1`
    
* deleteFriend($user_id, $user_id2):
    * $user_id : The user ID of an user who wants to remove someone from his friend list;
    * $user_id2 : The user ID of the removed friend
    `deleteFriend($_SESSION['id'], $userInfo['id'])`
    
    Database : 
    * `DELETE INTO friend`