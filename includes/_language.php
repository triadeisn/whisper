<?php
/**
 * Created by PhpStorm.
 * User: beauv
 * Date: 09/10/2018
 * Time: 10:44
 */

function findLang() {
    if (isset($_GET['lang'])) {
        return $_GET['lang'];
    }
    elseif (preg_match("/^fr/", $_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        return 'fr';
    }
    else {
        return 'en';
    }
}

$language = [
    'en' => [
        'language' => 'English',
        'projectname' => 'Whisper',
        'friend' => [
            'alreqse' => 'Friend request already sent.'
        ],
        
        'navbar' => [
            'profile' => 'Profile',
            'message' => 'Messages',
            'friend' => 'Friends'
        ],
        
        'main' => [
            'add_post' => 'Create a new post',
            'post' => 'Post'
        ],
        
        'modal' => [
            'profile' => [
                'header' => 'Profile'
            ],
            'message' => [
                'header' => 'Messages'
            ],
            'friend' => [
                'header' => 'Friends'
            ],
        ],
        
        'user' => [
            'registration' => [
                'registration' => 'Registration',
                'username' => 'Username',
                'email' => 'Email',
                'confirm_email' => 'Confirm email',
                'password' => 'Password',
                'confirm_password' => 'Confirm password',
                'register' => 'Register'
            ],
            
            'login' => [
                'logging' => 'Logging in',
                'username' => 'Username',
                'password' => 'Password',
                'register' => 'Register',
                'fill_out' => 'Fill out the form below to log in the website',
                'login' => 'Login'
            ],
        ],
    ],

    'fr' => [
        'language' => 'Français',
        'projectname' => 'Whisper',
        'friend' => [
            'alreqse' => 'Invitation déjà envoyée.'
            
        ],
        'navbar' => [
            'profile' => 'Profil',
            'message' => 'Messages',
            'friend' => 'Amis' 
            
        ],
        'main' => [
            'add_post' => 'Créer une nouvelle publication',
            'post' => 'Publier'
            
        ],
        'modal' => [            
            'profile' => [
                'header' => 'Profil'
            ],
            'message' => [
                'header' => 'Messages'
            ],
            'friend' => [
                'header' => 'Amis'
            ]
        ],
        'user' => [
            'registration' => [
                'registration' => 'Inscription',
                'username' => 'Pseudo',
                'email' => 'Email',
                'confirm_email' => 'Confirmer email',
                'password' => 'Mot de passe',
                'confirm_password' => 'Confirmer mot de passe',
                'register' => 'S\'inscrire'
            ],
            'login' => [
                'logging' => 'Connexion',
                'username' => 'Pseudo',
                'password' => 'Mot de passe',
                'register' => 'S\'inscrire',
                'fill_out' => 'Remplissez le formulaire ci-dessous pour se connecter à notre site',
                'login' => 'Se connecter'
            ]
        ]
    ]
];