<?php
	session_start();
	
	   include_once('../php/function.php');
	   include_once('../includes/init.php');
	
	if(isset($_POST['message-form']))
	{
	    sendMessage($_SESSION['id'], $_GET['id'], $_POST['message']);
	}	
	?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Message - <?php echo getUsername($_GET['id']); ?></title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
		<link rel="stylesheet" href="../css/style.css">
		<link rel="stylesheet" href="../css/chat-style.css">
        <script src="../../js/jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<header>
			<nav>
				<ul id="left">
					<li><a id="navbutton"><i class="fas fa-user"></i> Profile</a></li>
					<li><a id="myBtn" class="active"><i class="fas fa-comment-alt"></i> Message</a></li>
					<li><a href="#" id="navbutton"><i class="fas fa-user-friends"></i> Friend</a></li>
				</ul>
				<ul id="center">
					<li id="center">
						<a href="..">
						<img style="display: inline;" src="../img/logo.png">
						</a>
					</li>
				</ul>
			</nav>
		</header>
		<main>
			<section class="users">
				<?php
					$userid = lastChat($_SESSION['id']);
					for($i = 0; $i < count($userid); $i++) {
					?>
				<a id="userBtn"
					<?php
						if($userid[$i] == $_GET['id']) {
						    echo "class='active'";
						}
						else {
						echo "href='?id=" . $userid[$i] . "'";
						}
						?>
					>
					<ul class="user">
						<li>
							<img class="profile-picture" src="../<?php echo getProfilepicture($userid[$i]); ?>">
						</li>
						<li class="profile-information">
							<span class="username"><?php echo getUsername($userid[$i]); ?></span><br>
							<span class="message"><?php echo fetchLastMessage($_SESSION['id'], $userid[$i]); ?></span>
						</li>
					</ul>
				</a>
				<hr>
				<?php
					}
					?>
			</section>
			<section id="section_message" class="messages" onload="scrolldiv()">
                <ul id="message">
				<?php
					$fetch_allMsg = $db->prepare("SELECT * FROM `chat` WHERE (sender_id = :user1 AND receiver_id = :user2) OR (sender_id = :user2 AND receiver_id = :user1)");
					$fetch_allMsg->execute([
					    'user1' => $_SESSION['id'],
					    'user2' => $_GET['id']
					]);
										
					while($message = $fetch_allMsg->fetch()){
					    if(isSender($message['id'], $_SESSION['id'])) {
					?>
                    <li class="sender">
                        <ul>
                        <li>
                            <img class="profile-picture" src="../<?php echo getProfilepicture($message['sender_id']); ?>">
                        </li>
                        <li class="message-container">
                            <span><?php echo getUsername($message['sender_id']); ?></span>
                            <span class="message"><?php echo $message['message']; ?></span>
                        </li>
                        </ul>
                    </li>
                    <hr>
				<?php
					}
					else {
					?>
                    <li class="receiver">
                        <ul>
                        <li>
                            <img class="profile-picture" src="../<?php echo getProfilepicture($message['sender_id']); ?>">
                        </li>
                        <li class="message-container">
                            <span class="username"><?php echo getUsername($message['sender_id']); ?></span>
                            <span class="message"><?php echo $message['message']; ?></span>
                        </li>
                        </ul>
                    </li>
                    <hr>
				<?php
					}
					}
					?>
        </ul>
			</section>
			<section class="inputZone">
                <form method="post">
                    <ul>
                        <li class="test">
                            <div class="input" contenteditable id="editor"></div>
                        </li>
                        <li>
                            <input class="input_submit" type="submit" name="message-form" id="tes2">
                        </li>
                    </ul>
                </form>
			</section>
		</main>
        <script type="text/javascript">
            function validerForm(event){
                if(event.keyCode == 13 && !event.shiftKey) {
                    document.getElementById("message-form").submit();
                }
            };
            
            $('#tes2').click(function (){
                var post = document.getElementById("editor").innerHTML;
                var editor = document.getElementById('editor');
                var textarea = document.createElement('textarea');
                
                textarea.id = "result";
                textarea.value = post;
                textarea.name = "message";
                editor.replaceWith(textarea);
            });
            
            function scrolldiv() {
                document.getElementById('section_message').scrollTop = 1000;
            }
        </script>
	</body>
</html>