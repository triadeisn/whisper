<?php
session_start();

include_once('../php/function.php');
include_once('../includes/init.php');

echo $_POST['id'];

$fetch_allMsg = $db->prepare("SELECT * FROM `chat` WHERE (sender_id = :user1 AND receiver_id = :user2) OR (sender_id = :user2 AND receiver_id = :user1)");
$fetch_allMsg->execute([
    'user1' => $_SESSION['id'],
    'user2' => $_POST['id']
]);

while($message = $fetch_allMsg->fetch()){
    if(isSender($message['id'], $_SESSION['id'])) {
?>

<li class="sender">
    <ul>
        <li>
            <img class="profile-picture" src="../<?php echo getProfilepicture($message['sender_id']); ?>">
        </li>
        <li class="message-container">
            <span><?php echo getUsername($message['sender_id']); ?></span>
            <span class="message"><?php echo $message['message']; ?></span>
        </li>
    </ul>
</li>
<hr>
<?php
                                                  }
    else {
?>
<li class="receiver">
    <ul>
        <li>
            <img class="profile-picture" src="../<?php echo getProfilepicture($message['sender_id']); ?>">
        </li>
        <li class="message-container">
            <span class="username"><?php echo getUsername($message['sender_id']); ?></span>
            <span class="message"><?php echo $message['message']; ?></span>
        </li>
    </ul>
</li>
<hr>
<?php
         }
}