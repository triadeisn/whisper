$(document).ready(function() {
  // Giphy API defaults
  const giphy = {
    baseURL: "https://api.giphy.com/v1/gifs/",
    key: "dc6zaTOxFJmzC",
    tag: "fail",
    type: "random",
    rating: "pg-13"
  };
  // Target gif-wrap container
  const $gif_wrap = $("#gif-wrap");
  // Giphy API URL
  let giphyURL = encodeURI(
    giphy.baseURL +
    giphy.type +
    "?api_key=" +
    giphy.key +
    "&tag=" +
    giphy.tag +
    "&rating=" +
    giphy.rating
  );

  // Call Giphy API and render data
  var newGif = () => $.getJSON(giphyURL, json => renderGif(json.data));

  // Display Gif in gif wrap container
  var renderGif = _giphy => {
    // Set gif as bg image
    $gif_wrap.css({
      "background-image": 'url("' + _giphy.image_original_url + '")'
    });
  };

  // Call Giphy API for new gif
  newGif();
});
