<?php
session_start();
$_SESSION['id'] = 2;
include_once("../php/chat.php");
if(isset($_POST['sendMsg']))
{
    sendMessage($_SESSION['id'], $_GET['id'], $_POST['message']);
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Messaging Interface</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="wrapper">
        <div class="container">
            <aside class="users">
                <header>
                    <h1>Messages</h1>
                </header>
                <ul>
                    <li>
                        <div class="avatar top">
                            <img src="../img/2.jpg" alt="">
                            <div class="online"></div>
                        </div>
                        <div class="users-list">
                            <div class="username">
                                <p><a href="?id=2">Tiburce Aubert</a></p>
                            </div>
                            <div class="text">
                                <p>Can't wait to see you!</p>
                            </div>
                        </div>
                        <div class="timestamp">
                            <p>20:32</p>
                        </div>
                    </li>
                    <li>
                        <div class="avatar top">
                            <img src="../img/1.jpg" alt="">
                            <div class=" online offline"></div>
                        </div>
                        <div class="users-list">
                            <div class="username">
                                <p><a href="?id=1">Alan Beauvais</a></p>
                            </div>
                            <div class="text">
                                <p>Did you buy it?</p>
                            </div>
                        </div>
                        <div class="timestamp">
                            <p>21:42</p>
                        </div>
                    </li>
                    <li>
                        <div class="avatar top">
                            <img src="../img/3.jpg" alt="">
                            <div class="online offline"></div>
                        </div>
                        <div class="users-list">
                            <div class="username">
                                <p><a href="?id=3">Valentin Huault</a></p>
                            </div>
                            <div class="text">
                                <p>Nothing yet...</p>
                            </div>
                        </div>
                        <div class="timestamp">
                            <p>22:18</p>
                        </div>
                    </li>
                </ul>
            </aside>
            <section id="chat-screen">
                <section id="messages">
                    <?php
            global $db;
            $fetch_msg = $db->prepare('
            select * from `chat` where (`sender_id` = :sender and `receiver_id` = :receiver) or (`sender_id` = :receiver and `receiver_id` = :sender)
            ');
            $fetch_msg->execute([
                'sender' => $_SESSION['id'],
                'receiver' => $_GET['id']
            ]);
            while($message = $fetch_msg->fetch()) {
                if (isSender($message['id'], $_SESSION['id'])) {
                    ?>
            <article class="right">
                        <div class="avatar">
                            <img src="../img/1.jpg" alt="">
                        </div>
                        <div class="msg">
                            <div class="pointer"></div>
                            <div class="inner-msg">
                                <p><?php echo $message['message']; ?></p>
                            </div>
                        </div>
                    </article>
            <?php
                }
                else {
                    ?>
                    <article>
                        <div class="avatar">
                            <img src="../img/2.jpg" alt="">
                        </div>
                        <div class="msg">
                            <div class="pointer"></div>
                            <div class="inner-msg">
                                <p><?php echo $message['message'];?></p>
                            </div>
                        </div>
                    </article>
                    <?php
                }
            }
            ?>
                </section>
                <div class="msg-compose">
                    <i class="fas fa-paperclip"></i>
                    <form method="post">
                        <textarea name="message" placeholder="Say something..."></textarea>
                        <input name="sendMsg" value="<i class='fab fa-telegram-plane'></i>" type="submit">
                    </form>
                </div>
            </section>
        </div>
    </div>
</body>
</html>

