<?php
/**
 * Created by PhpStorm.
 * User: beauv
 * Date: 13/11/2018
 * Time: 11:11
 */
session_start();
include_once("C:/wamp64/www/isn/includes/init.php");
include("C:/wamp64/www/isn/php/gameActivity.php");

$_SESSION['id'] = 1;

if(isset($_POST['form'])) {
    createGameEvent($_SESSION['id'], $_POST['game'], $_POST['event']);
}
?>
<html>
    <head>
        <title>ISN</title>
        <meta charset="utf-8" name="viewport" content="width=device-witch, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../css/master.css">
        <script src="../../js/jquery-3.3.1.min.js"></script>
        <script src="../js/scroll.js"></script>
    </head>
    <body>
        <header class="header">
            <nav>
                <span class="logo"><img src="../res/logo.png"></span>
                <form method="get" class="search-box">
                    <input class="search-txt" type="text" name="search" placeholder="Type to search">
                    <button class="search-btn" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </form>
                <input class="menu-btn" type="checkbox" id="menu-btn" />
                <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
                <ul class="menu">
                    <?php if (!isset($_SESSION['id'])){ ?>
                    <li><a href="t/signin.php <?php if(isset($_GET['lang'])) { echo "?lang=" . $_GET['lang']; }?>"><?php echo $language[findLang()]['menu']['signin'] ?></a></li>
                    <li><a href="t/signup.php<?php if(isset($_GET['lang'])) { echo "?lang=" . $_GET['lang']; } ?>"><?php echo $language[findLang()]['menu']['signup'] ?></a></li>
                    <?php } else { ?>
                    <li><a href="t/profile.php<?php if(isset($_GET['lang'])) { echo "?lang=" . $_GET['lang']; }?>"><?php echo $language[findLang()]['menu']['profile'] ?></a></li>
                    <li><a href="t/friend.php <?php if(isset($_GET['lang'])) { echo "?lang=" . $_GET['lang']; }?>"><?php echo $language[findLang()]['menu']['friend'] ?></a></li>
                    <li><a href="t/signout.php<?php if(isset($_GET['lang'])) { echo "?lang=" . $_GET['lang']; } ?>"><?php echo $language[findLang()]['menu']['signout'] ?></a></li>
                    <?php } ?>
                </ul>
            </nav>
        </header>
        <main>
            <form method="post">
                <select name="game">
                    <?php
                    global $db;
                    $fetchGame = $db->query('SELECT * FROM `game`');
                    while ($game = $fetchGame->fetch()) {
                    ?>
                    <option value="<?php echo $game['id']; ?>"><?php echo $game['name']; ?></option>
                    <?php
                    }
                    ?>
                </select><br>
                <select name="event">
                    <option value="1">Fallen kingdom</option>
                    <option value="2">Braquage</option>
                </select><br>
                <input type="submit" name="form">
            </form>
        </main>
        <footer>
            <form method="get" id="langue_change">
                <select name="lang" onchange="changeLanguage()">
                    <?php if (isset($_GET['lang']) AND $_GET['lang'] == 'en') { ?>
                    <option value="en">English</option>
                    <option value="fr">Français</option>
                    <?php } else { ?>
                    <option value="fr">Français</option>
                    <option value="en">English</option>
                    <?php } ?>
                </select>
            </form>
        </footer>
        <script type="text/javascript">
            function changeLanguage() {
                document.getElementById("langue_change").submit();
            }
        </script>
    </body>
</html>    
